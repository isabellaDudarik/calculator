package com.example.calculator2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText firstNumber;
    EditText secondNumber;
    EditText operation;

    Button calc;
    TextView result;
    Switch accurancy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstNumber = findViewById(R.id.number1);
        secondNumber = findViewById(R.id.number2);
        operation = findViewById(R.id.operation);

        calc = findViewById(R.id.calc);
        calc.setOnClickListener(this::calc);

        result = findViewById(R.id.result);
        accurancy = findViewById(R.id.accurency);


    }

    public void calc(View v) {

        int first = Integer.parseInt(firstNumber.getText().toString());
        int second = Integer.parseInt(secondNumber.getText().toString());
        String oper = operation.getText().toString();

        switch (oper) {
            case ("/"):
                if (second == 0) {
//            Toast.makeText(this, "Second value can't be 0", Toast.LENGTH_SHORT).show();
                    new AlertDialog.Builder(this)
                            .setTitle("Error")
                            .setMessage("Second value can't be 0")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    secondNumber.setText("");
                                }
                            })
                            .show();
                } else {
                    double r = ((double) first / (double) second);
                    result.setText(accurancy.isChecked() ? String.valueOf(r) :
                            String.valueOf((int) r));
                }
                break;
            case ("*"):
                result.setText(String.valueOf(first * second));
                break;
            case ("+"):
                result.setText(String.valueOf(first + second));
                break;
            case ("-"):
                result.setText(String.valueOf(first - second));
                break;
        }
    }
}
